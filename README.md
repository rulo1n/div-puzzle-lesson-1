<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>DivPuzze1</title>
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <h1>DIV PUZZLE1</h1>

  <div class="top">#141f40 </div>
  <div class="middle1">#80bfa8</div>
  <div class="middle2">#8c2727</div>
  <div class="bottom">#d98d30</div>

</body>
</html>